const electron = require('electron'),
path = require('path'),
ipc = electron.ipcRenderer,
dialog = electron.dialog,
BrowserWindow = electron.BrowserWindow;

ipc.on('set_timer',(e,o)=>{
    console.log(o);
    console.log('Setting Times');
    create_timer(o);
    ipc.send('timer_set');
})

function close_timer(){
    ipc.send('close');
}