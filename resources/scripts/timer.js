const zone = document.getElementById('timer');
let tmr;

function create_timer(obj){
	console.log("Timerssss");
	tmr = new Timers(obj);
}

class Timers{
	constructor(obj){
		this.hrs = obj.hrs ? obj.hrs : 0;
		this.min = obj.mins ? obj.mins : 0;
		this.sec = obj.secs ? obj.secs : 0;
		this.id = `timers`;
		this.clr = obj.color;
		this.finished = false;
		this.state = true;
		this.title = obj.title;
		this.set_time(this.min,this.sec);
		this.create_timer();
		this.update_time();
	}

    set_time(min,sec){
		console.log(this.title);
		if(this.min >=60){
			this.hrs += Math.floor(this.min/60);
			this.min -= (Math.floor(this.min/60))*60;
		}
		if(this.sec >=60){
			this.min += Math.floor(this.sec/60);
			this.sec -= (Math.floor(this.sec/60))*60;
		}
	}

	create_timer(){
		let div = document.createElement("div"),
		ttl = document.createElement("div"),
		tmz = document.createElement("div"),
		sec = document.createElement("p"),
		min = document.createElement("p"),
		hrs = document.createElement("p"),
		title = document.createElement("p"),
		close = document.createElement("button");

		close.innerHTML = "<i class='material-icons'>close</i>";
		hrs.innerHTML = this.hrs ? ((this.hrs < 10) ? `0${this.hrs}:` : `${this.hrs}:`) : '00:';
		min.innerHTML = this.min ? ((this.min < 10) ? `0${this.min}:` : `${this.min}:`) : '00:';
		sec.innerHTML = this.sec ? ((this.sec < 10) ? `0${this.sec}` : this.sec) : '00';
		title.innerHTML = this.title;
		
		hrs.id = this.id + "_hrs";
		min.id = this.id + "_min";
		sec.id = this.id + "_sec";
		title.id = this.id + "_title";
		close.id = this.id + "_close";

		hrs.className = min.className = sec.className = "timd";
		ttl.className = "titl";
		tmz.className = "tz";
		close.className = "btn";
		close.setAttribute("onclick","end()");

		ttl.appendChild(title);
		tmz.appendChild(hrs);
		tmz.appendChild(min);
		tmz.appendChild(sec);

		div.appendChild(ttl);
		div.appendChild(tmz);
		
		
		div.id = this.id;
		div.style.backgroundColor = this.clr;
		div.classList.add("timer");
		div.setAttribute("onclick","toggle_timer()");
		zone.appendChild(close);
		zone.appendChild(div);
	}

	display(){
		let hr = document.getElementById(`${this.id}_hrs`);
		let min = document.getElementById(`${this.id}_min`);
		let sec = document.getElementById(`${this.id}_sec`);


		hr.innerHTML = (this.hrs < 10) ? `0${this.hrs}:` : `${this.hrs}:`;
		min.innerHTML = (this.min < 10) ? `0${this.min}:` : `${this.min}:`;
		sec.innerHTML = (this.sec < 10) ? `0${this.sec}` : this.sec;
	}

	update_time(){
		if(this.finished || !this.state){
			clearInterval(this.timer);
			return;
		}
		let timer = setInterval(_=>{
			if(this.min !== 0 || this.sec !== 0 || this.hrs !== 0){
				if(this.sec === 0 && this.min !== 0){
					this.sec = 59;
					this.min--;
				}
				else if((this.min === 0 && this.sec === 0) && this.hrs !== 0){
					this.hrs--;
					this.min = 59;
					this.sec = 59;
				}
				else{
					this.sec--;
				}
			}
			else{
				this.finished = true;
				this.state = false;
				return;
			}
			this.display();
		},1000);
		this.timer = timer;
	}

}


function end(){
	close_timer();
}

function toggle_timer(){
	console.log(tmr);
	tmr.state = !(tmr.state);
	tmr.update_time();
}