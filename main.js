const electron = require('electron'),
path = require('path'),
app = electron.app,
globalShortcut = electron.globalShortcut,
dialog = electron.dialog,
ipc = electron.ipcMain,
BrowserWindow = electron.BrowserWindow,
Menu = electron.Menu;
// screen = electron.screen;
//Other Imports

let main_window,
windows = [],
index = windows.length;

require('electron-reload')(__dirname);

app.on('ready',open_main);

const temp = [{
    label : 'Main',
    submenu : [{
        label : 'Quit',
        role : 'Close',
        accelerator : 'CmdOrCtrl+Backspace'
    },
    {
        label : 'New Timer',
        accelerator : 'Alt+N',
        enabled : 'false'
    }]
},
{
    label : 'About',
    submenu : [{
        label : 'Github',
    }]
},
{
    label : 'Dev',
    submenu : [{
        label : 'open dev tools',
        accelerator : 'Ctrl+Shift+I',
        click : _=>{
            main_window.webContents.openDevTools();
        }
    }]
}]

function open_main(){
    main_window = new BrowserWindow({
        width : 400,
        height : 600,
        x : 975,
        y : 125,
        movable : false,
        maximizable : false
    })

    reg_shortcuts();
    create_menu();
    main_window.loadURL(path.join('file:///',__dirname,'index.html'));
    main_window.show();
}


function reg_shortcuts(){
    globalShortcut.register('CmdOrCtrl+Backspace',_=>{
        app.quit();
    })
}

function create_menu(){
    let menu = Menu.buildFromTemplate(temp);
    Menu.setApplicationMenu(menu);
}

ipc.on('timer_created',(e,o)=>{
  let timer = new BrowserWindow({
        width : 400,
        height : 200,
        movable : true,
        alwaysOnTop : true,
        focusable : true,
        resizable : false,
        thickFrame : true
    })
    timer.loadURL(path.join('file:///',__dirname,'timer.html'));
    timer.webContents.on('did-finish-load',_=>{
        timer.webContents.send('set_timer',o);
    });
    windows.push(timer);
    ipc.on('timer_set',_=>{
        timer.show();
    })
})


function show_win(){
    let body = '';
    windows.forEach(i=>{
        body += i.id +' / ';
    })
    dialog.showMessageBox({
        title : 'timers',
        message : body
    })
}


ipc.on('close',(e)=>{
    let ele = windows.find(i=>{
        return i.id === e.sender.id;
    });

    windows = windows.filter(i=>{
        return i.id !== e.sender.id;
    });
    ele.close();
})